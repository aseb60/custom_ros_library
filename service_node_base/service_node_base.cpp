/**************************************************
 * @file ServiceNodeBase
 * @author Sergej Ellenschläger, Ameline Seba
 * @version v1.0
 * @date ${date}
 * @brief: Module for using the periphery xyz
 **************************************************
 */
/* Includes */
#include "service_node_base.hpp"
/* Preprocessor defines */
/* Preprocessor macros */
/* Module intern type definitions */
/* Static module variables */
/* Static module functions (prototypes) */
/* Public functions */
/* Private class functions*/
/* Protected class functions*/
/* Public class class functions*/
/**
 * @brief Construct a new Service Node Base object.
 * @param service_node_name is a name for a new service node.
 */
ServiceNodeBase::ServiceNodeBase(const char* service_node_name) : rclcpp::Node(service_node_name)
{
}

bool ServiceNodeBase::implement_node(std::shared_ptr<ServiceNodeBase> node) {
  RCLCPP_DEBUG(rclcpp::get_logger("rclcpp"), "debug: no error %s", typeid(node).name());
  return true;
}

/**
 * @brief Initializes node by calling implement_node and handles ros.
 * @note Creates a shared_ptr from this class and passes it to implement_node.
 * @param None
 * @return None
 */
void ServiceNodeBase::process_node(void)
{
  // create a shared_ptr from this class and overrides its deleter, because there is no memory allocation
  std::shared_ptr<ServiceNodeBase> node(this, [](ServiceNodeBase*) {});
  // calls implement_node to create the necessary services, publisher, etc.
  if (!implement_node(node)) {
    RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Error: error on implementing node");
    return;
  }
  else {
    RCLCPP_DEBUG(rclcpp::get_logger("rclcpp"), "debug: node can run");
  }
  // handling ros
  rclcpp::spin(node);
}

/* Static module functions (implementation) */
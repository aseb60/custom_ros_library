# custom_ros_library
This library is used in two parallel projects:
- [PIRO](https://git.thm.de/institut-f-r-technik-und-informatik/master-projektseminar/industrierobotik/sose-24/laserscanner-port/portierung-laser-scanner)
- [PVES](https://git.thm.de/micro-citicar/pves-czekansky-ss24/stm32-mros-testproject)

![custom ros library uml](models/custom_ros_library_model.JPG)

## How to add in your CMakeList.txt
Add this line to your target_inlcude_directories(...):
```
$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/custom_ros_library>
```

Add this line to a service_node executable in your add_executable(...):
```
custom_ros_library/service_node_base/service_node_base.cpp
```
## How to use

### ClientNodeBase

Create a new c++ class, which inherrits ClientNodeBase.
Implement prepare_request to send information to service and call its parent method last.
Implement prepare_request to send information to service and call its parent method last.

```
/**
 * @brief Construct a new Client Calibration:: Client Calibration object
 *
 */
ClientCalibration::ClientCalibration() : ClientNodeBase(CALIBRATE_SCANNER_NODE, CALIBRATE_SCANNER_SERVICES)
{
}

/**
 * @brief It can be used to prepare a request to CALIBRATE_SCANNER_SERVICES.
 * @note Calls the parent function.
 * @param request is a trigger request pointer
 * @return true on success
 * @return false on fail
 */
bool ClientCalibration::prepare_request(shared_ptr<Trigger::Request> request)
{
    RCLCPP_DEBUG(rclcpp::get_logger("rclcpp"), "Debug: no need to prepare for %s", CALIBRATE_SCANNER_SERVICES);
    return ClientNodeBase::prepare_request(request);
}

/**
 * @brief It is called when the service call process was successful on CALIBRATE_SCANNER_SERVICES and checks the result.
 * @note Calls the parent function.
 * @param result is trigger responds pointer
 * @return true on success
 * @return false on fail
 */
bool ClientCalibration::process_result(shared_ptr<Trigger::Response> result)
{
    if (result->success)
    {
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Info: %s", result->message.c_str());
    }
    else
    {
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Info: failed to call %s", CALIBRATE_SCANNER_SERVICES);
        return false;
    }

    return ClientNodeBase::process_result(result);
}

```


### ClientNodeBase

Create a new c++ class, which inherrits ClientNodeBase.
Implement implemnt_node as new node.

```
bool CameraNodeInterface::implement_node(std::shared_ptr<ServiceNodeBase> node)
{

    this->camera_node_interface_subscription_ = this->create_subscription<interfaces::msg::ImagePair>(IMAGE_PAIR_PUBLISHER_INTERFACE, 10, std::bind(&CameraNodeInterface::camera_node_interface_subscriber, this, _1));
    subscription_ = this->create_subscription<std_msgs::msg::String>(
        "topic", 10, std::bind(&CameraNodeInterface::topic_callback, this, _1));
    return CameraNodeRPI::implement_node(node);
}


```
/**************************************************
 * @file ServiceNodeBase
 * @author Ameline Seba
 * @version v1.0
 * @date 17.05.2024
 * @brief: Module for creating a basic service.
 **************************************************
 */

#ifndef INC_SERVICE_NODE_BASE_H_
#define INC_SERVICE_NODE_BASE_H_
/* Includes */
#include "rclcpp/rclcpp.hpp"
#include <memory>
#include <list>
/* namespace abbreviations */
/* Public Preprocessor defines */
/* Public Preprocessor macros */
/* Public type definitions */
/**
 * @brief A base class for creating services.
 *
 */
class ServiceNodeBase : public rclcpp::Node
{
protected:
  /**
   * @brief A list to all implemented services.
   */
  std::list<std::shared_ptr<rclcpp::ServiceBase>> services_;

  /**
   * @brief Construct a new Service Node Base object.
   * @param service_node_name is a name for a new service node.
   */
  ServiceNodeBase(const char* service_node_name);

  /**
   * @brief Shall implement all node specific things.
   * @note It gets called by process_node.
   * @param node is a shared_ptr from this class
   * @return None
   */
  virtual bool implement_node(std::shared_ptr<ServiceNodeBase> node);

public:
  /**
   * @brief Initializes node by calling implement_node and handles ros.
   * @note Creates a shared_ptr from this class and passes it to implement_node.
   * @param None
   * @return None
   */
  virtual void process_node(void);
};
/* Public functions (prototypes) */
/* Private class functions (prototypes) */
/* Protected class functions (prototypes) */
/* Public class class functions (prototypes) */
#endif /* INC_SERVICE_NODE_BASE_H_ */
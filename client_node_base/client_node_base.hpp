/**************************************************
 * @file {{name}}
 * @author Ameline Seba
 * @version v1.0
 * @date ${date}
 * @brief: Module for using the periphery xyz
 **************************************************
 */

#ifndef INC_CLIENT_NODE_BASE_H_
#define INC_CLIENT_NODE_BASE_H_
/* Includes */
#include "rclcpp/rclcpp.hpp"
#include <chrono>
#include <cstdlib>
#include <memory>

using namespace std::chrono_literals;
/* namespace abbreviations */
/* Public Preprocessor defines */
/* Public Preprocessor macros */
/* Public type definitions */

/**
 * @brief A base class for implementing ros clients
 * @note
 * @tparam  ServiceInterface is a ros service class
 * @tparam  ServiceInterfaceRequest is the request from the ros service class
 * @tparam  ServiceInterfaceResponse is the response from the ros service class
 */
template <class ServiceInterface, class ServiceInterfaceRequest, class ServiceInterfaceResponse>
class ClientNodeBase : public rclcpp::Node
{
public:
  const char *client_interface_name;

  ClientNodeBase(const char *client_node_name, const char *client_interface_name);
  virtual bool check(int arg);
  virtual bool prepare_request(std::shared_ptr<ServiceInterfaceRequest> request);
  virtual bool prepare_request(std::shared_ptr<ServiceInterfaceRequest> request, char **argv);
  virtual bool process_result(typename rclcpp::Client<ServiceInterface>::FutureAndRequestId *result);
  virtual bool process_result(
      std::shared_ptr<ServiceInterfaceResponse> result);

  virtual void process_node(int argc, char **argv);
  virtual void process_node(void);
};

// in c++ generics "source code" is placed in header files or a include has to be used in the folloing line,
// but someone might list this generics file to add_executable, which will fail.

/**
 * @brief Construct a new Client Node Base< Service Interface,  Service Interface Request,  Service Interface Response>:: Client Node Base object
 *
 * @tparam ServiceInterface
 * @tparam ServiceInterfaceRequest
 * @tparam ServiceInterfaceResponse
 * @param client_node_name is a string to a node to connect with
 * @param client_interface_name is the provided service from the connected node
 */
template <class ServiceInterface, class ServiceInterfaceRequest, class ServiceInterfaceResponse>
ClientNodeBase<ServiceInterface, ServiceInterfaceRequest, ServiceInterfaceResponse>::ClientNodeBase(
    const char *client_node_name, const char *client_interface_name)
    : rclcpp::Node(client_node_name)
{
  this->client_interface_name = client_interface_name;
}

/**
 * @brief This is a default function and is the first method for checking node parameters.
 * @note It prints basic information to ros debug. Call this function from your derived class.
 * @tparam ServiceInterface
 * @tparam ServiceInterfaceRequest
 * @tparam ServiceInterfaceResponse
 * @param argc is the amount of arguments passed
 * @return true is always returned
 * @return false can be returned on errors in child classes
 */
template <class ServiceInterface, class ServiceInterfaceRequest, class ServiceInterfaceResponse>
bool ClientNodeBase<ServiceInterface, ServiceInterfaceRequest, ServiceInterfaceResponse>::check(int argc)
{
  if (argc == 0)
  {
    RCLCPP_DEBUG(rclcpp::get_logger("rclcpp"), "debug: no arguments were passed");
  }
  RCLCPP_DEBUG(rclcpp::get_logger("rclcpp"), "debug: no error found");
  return true;
}

/**
 * @brief This is a default function and prepares a request based of the request type.
 * @note It prints basic information to ros debug. Call this function last from your derived class.
 * @tparam ServiceInterface
 * @tparam ServiceInterfaceRequest
 * @tparam ServiceInterfaceResponse
 * @param argc is the amount of arguments passed
 * @return true is always returned
 * @return false can be returned on errors in child classes
 */
template <class ServiceInterface, class ServiceInterfaceRequest, class ServiceInterfaceResponse>
bool ClientNodeBase<ServiceInterface, ServiceInterfaceRequest, ServiceInterfaceResponse>::prepare_request(
    std::shared_ptr<ServiceInterfaceRequest> request)
{
  RCLCPP_DEBUG(rclcpp::get_logger("rclcpp"), "request type: %s", typeid(request).name());
  return true;
}

/**
 * @brief This is a default function and prepares a request based of the request type and passed arguments from cli.
 * @note It prints basic information to ros debug. Call this function last from your derived class.
 * @tparam ServiceInterface
 * @tparam ServiceInterfaceRequest
 * @tparam ServiceInterfaceResponse
 * @param argc is the amount of arguments passed
 * @return true is always returned
 * @return false can be returned on errors in child classes
 */
template <class ServiceInterface, class ServiceInterfaceRequest, class ServiceInterfaceResponse>
bool ClientNodeBase<ServiceInterface, ServiceInterfaceRequest, ServiceInterfaceResponse>::prepare_request(
    std::shared_ptr<ServiceInterfaceRequest> request, char **argv)
{
  // checks for passed arguments from cli
  if (argv == NULL)
  {
    RCLCPP_DEBUG(rclcpp::get_logger("rclcpp"), "comand line: no arguments were passed");
  }
  else
  {
    // lists all cli arguments
    for (int i = 0; *(i + argv) != NULL; i++)
    {
      RCLCPP_DEBUG(rclcpp::get_logger("rclcpp"), "comand line arg[%i]: %s", i, *(i + argv));
    }
  }

  return prepare_request(request);
}

/**
 * @brief This is a default function and processes a result based of the response type.
 * @note It prints basic information to ros debug. Call this function last from your derived class.
 * @tparam ServiceInterface
 * @tparam ServiceInterfaceRequest
 * @tparam ServiceInterfaceResponse
 * @param result is a std::shared_ptr<ServiceInterfaceResponse>
 * @return true is always returned
 * @return false can be returned on errors in child classes
 */
template <class ServiceInterface, class ServiceInterfaceRequest, class ServiceInterfaceResponse>
bool ClientNodeBase<ServiceInterface, ServiceInterfaceRequest, ServiceInterfaceResponse>::process_result(
    std::shared_ptr<ServiceInterfaceResponse> result)
{
  RCLCPP_DEBUG(rclcpp::get_logger("rclcpp"), "result type: %s", typeid(result).name());
  return true;
}

/**
 * @brief This is a default function and processes a result based of the response type.
 * @note It prints basic information to ros debug. Call this function last from your derived class.
 * @tparam ServiceInterface
 * @tparam ServiceInterfaceRequest
 * @tparam ServiceInterfaceResponse
 * @param result is a pointer of rclcpp::Client<ServiceInterface>::FutureAndRequestId and is valid once when calling get
 * @return true is always returned
 * @return false can be returned on errors in child classes
 */
template <class ServiceInterface, class ServiceInterfaceRequest, class ServiceInterfaceResponse>
bool ClientNodeBase<ServiceInterface, ServiceInterfaceRequest, ServiceInterfaceResponse>::process_result(
    typename rclcpp::Client<ServiceInterface>::FutureAndRequestId *result)
{
  RCLCPP_DEBUG(rclcpp::get_logger("rclcpp"), "result type: %s", typeid(result).name());
  return process_result(result->get());
}

/**
 * @brief It unifies the creation of client and prints useful debug information.
 * @tparam ServiceInterface
 * @tparam ServiceInterfaceRequest
 * @tparam ServiceInterfaceResponse
 * @param argc is the amount of passed arguments from the cli
 * @param argv is the argument list from the cli
 * @return None
 */
template <class ServiceInterface, class ServiceInterfaceRequest, class ServiceInterfaceResponse>
void ClientNodeBase<ServiceInterface, ServiceInterfaceRequest, ServiceInterfaceResponse>::process_node(int argc,
                                                                                                       char **argv)
{
  if (!check(argc))
  {
    RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "error: error on check");
    return;
  }
  // create a shared_ptr from this class and overrides its deleter, because there is no memory allocation
  std::shared_ptr<ClientNodeBase> node(this, [](ClientNodeBase *) {});
  // the last comment saved it: https://github.com/ros-navigation/navigation2/issues/3447
  typename rclcpp::Client<ServiceInterface>::SharedPtr client =
      node->template create_client<ServiceInterface>(this->client_interface_name);
  std::shared_ptr<ServiceInterfaceRequest> request = std::make_shared<ServiceInterfaceRequest>();
  // prepare a request and check for error
  if (!prepare_request(request, argv))
  {
    RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "error: error on preparing request");
    return;
  }
  // create connection to service
  while (!client->wait_for_service(1s))
  {
    if (!rclcpp::ok())
    {
      RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
    }
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
  }

  // Wait for the result.
  typename rclcpp::Client<ServiceInterface>::FutureAndRequestId result = client->template async_send_request(request);
  if (rclcpp::spin_until_future_complete(node, result) == rclcpp::FutureReturnCode::SUCCESS)
  {
    // error on process result
    if (!process_result(&result))
    {
      RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "error: error on processing result");
      return;
    }
  }
  else
  {
    RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Failed to call service");
  }
}

/**
 * @brief It unifies the creation of client and prints useful debug information by calling a overloaded method.
 * @tparam ServiceInterface
 * @tparam ServiceInterfaceRequest
 * @tparam ServiceInterfaceResponse
 * @return None
 */
template <class ServiceInterface, class ServiceInterfaceRequest, class ServiceInterfaceResponse>
void ClientNodeBase<ServiceInterface, ServiceInterfaceRequest, ServiceInterfaceResponse>::process_node(void)
{
  process_node(0, NULL);
}
/* Public functions (prototypes) */
/* Private class functions (prototypes) */
/* Protected class functions (prototypes) */
/* Public class class functions (prototypes) */
#endif /* INC_CLIENT_NODE_BASE_H_ */